const Helper = codecept_helper;

class MyHelper extends Helper {

    // before/after hooks
    _before() {
        // remove if not used
    }

    _after() {
        // remove if not used
    }

    // add custom methods here
    // If you need to access other helpers
    // use: this.helpers['helperName']

    async executeJS(fn) {
        return await this.helpers.Puppeteer.page.evaluate(fn);
    }

    listenConsole() {
        this.helpers.Puppeteer.page.on('console', async msg => {
            for (let i = 0; i < msg.args().length; i++) {
                let res = msg.args()[i]
                if (res instanceof Promise) {
                    res = await res
                }
                console.log('CONSOLE', res.jsonValue())
            }
        });
    }

}

module.exports = MyHelper;