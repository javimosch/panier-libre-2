import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
//import funql from 'funql-api/client.cjs'

Vue.use(Vuex)

//const fql = funql(process.env.VUE_APP_ENDPOINT)

export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {
        async createUser(props, data) {
            /*
            let res = await fql('registerAccount', [data], {
                namespace: 'user'
            })
            return res.value
            */
            let res = (await axios.post(process.env.VUE_APP_ENDPOINT + `/users`, data)).data
            console.warn(res)
            return res
        }
    },
    modules: {}
})