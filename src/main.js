/* import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
 */

import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import store from './store'

Vue.config.productionTip = false

Vue.use(VueRouter)

// Export factory function
export default function createApp() {
    // 1. Create a router instance
    const router = new VueRouter({
        mode: 'history',
        routes: [{
                path: '/',
                component: () =>
                    import ('./views/Home.vue'),
                meta: {
                    ssr: true
                }
            },
            {
                name: 'users',
                path: '/users',
                component: () =>
                    import ('./views/Users.vue'),
                meta: {
                    ssr: true
                }
            },
            {
                name: 'create-user',
                path: '/create-user',
                component: () =>
                    import ('./views/CreateUser.vue'),
                meta: {
                    ssr: true
                }
            }
        ]
    })

    // 2. Create a app instance
    const app = new Vue({
        router,
        store,
        // This is necessary, it is for vue-meta
        head: {},
        render: h => h(App)
    })

    // 3. return
    return { app, router }
}